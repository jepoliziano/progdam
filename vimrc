" ===============
"  INIT SETTINGS
" ===============

syntax on
set title
set history=500
set noerrorbells


" ===============
"  FILE SETTINGS
" ===============

"" General settings
set incsearch
set nohlsearch
set number
set showmatch
set wrap
set tw=76

" Status-bar and command-line settings
set ruler
set wildmenu
set laststatus=2
set statusline=%<\ %n:\ %f\ %m%r\ \ %y\ %=\ [%c\,%l]\ \(%L\ lines\)\ \ %P\ %<

" Splits
set splitbelow
set splitright

" No backup files
set noswapfile
set nobackup

" Modify indenting settings
set autoindent
set expandtab
set shiftwidth=4
set softtabstop=4

" Modify some other settings about files
set backspace=2

" Folding
set foldlevel=99


" ============================
"  SPECIFIC FILETYPE SETTINGS
" ============================

" Indenting settings for certain files
au FileType html,xml setlocal sw=2 sts=2 tw=100 colorcolumn=100
au FileType c,cpp,css,javascript,php setlocal sw=4 sts=4 tw=80
au FileType make setlocal sw=8 sts=8 tw=80
au FileType python setlocal sw=4 sts=4 tw=79
au FileType text setlocal sw=4 sts=4 tw=100 formatoptions=twa2 colorcolumn=100
au FileType java setlocal sw=4 sts=4 tw=100 colorcolumn=100

" Syntax settings for certain file extensions
au BufRead,BufNewFile *.s,*.asm set ft=nasm


" ===============
"  COLORS AND UI
" ===============

set background=dark

" ===========
"  FUNCTIONS
" ===========

" Change to file directory
func! ChangeToFileDir()
    exec 'cd %:p:h'
    exec 'pwd'
endfunc
command! ChangeToFileDir call ChangeToFileDir()

" ToggleColorColumn
func! ToggleColorColumn()
    if (&ft=='html' || &ft=='xml' || &ft=='java' || &ft=='txt')
        if &colorcolumn == 100
            set colorcolumn=0
            set textwidth=0
        else
            set colorcolumn=100
            set textwidth=99
        endif
    else
        if &colorcolumn == 80
            set colorcolumn=0
            set textwidth=0
        else
            set colorcolumn=80
            set textwidth=79
        endif
    endif
endfunc
command! ToggleColorColumn call ToggleColorColumn()

" ToggleRelativeNumber
func! ToggleRelativeNumber()
    if &relativenumber == 1
        set norelativenumber
        set number
    else
        set relativenumber
    endif
endfunc
command! ToggleRelativeNumber call ToggleRelativeNumber()


" =========
"  MAPPING
" =========

let mapleader=","

" Autoformat
map <leader>af :Autoformat<CR>

" Buffers
map <C-j> :bprevious!<CR>
map <C-k> :bnext!<CR>

" Change to file directory
nmap <leader><F7> :call ChangeToFileDir()<CR>

" Openterm
map <silent><C-h> :term<CR>

" Redraw vim
map <leader>re :redraw!<CR>

" Reload vimrc
map <leader>so :so ~/.vimrc<CR>

" Toggle hlsearch
map <silent> <leader>hl :set hlsearch! hlsearch?<CR>

" ToggleColorColumn
nmap <F6> :call ToggleColorColumn()<CR>
imap <F6> <Esc>:call ToggleColorColumn()<CR>a

" ToggleRelativeNumber
nmap <leader><F6> :call ToggleRelativeNumber()<CR>
imap <leader><F6> <Esc>:call ToggleRelativeNumber()<CR>a

" ToggleWrapLine
nmap <F7> :set nowrap! wrap?<CR>
imap <F7> <Esc>:set nowrap! wrap?<CR>a

" Trailing lines
nmap <silent> <leader><F10> :%s/\s\+$//e<CR>
imap <silent> <leader><F10> <Esc> :%s/\s\+$//e<CR>a
