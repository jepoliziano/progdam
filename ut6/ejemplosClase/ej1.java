// Hola mundo gráfico con SWING

import java.awt.*;
import javax.swing.*;

public class ej1
{
	public static void main(String[] args) {
		JFrame frame = new JFrame("Hola mundo");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
		JLabel jl = new JLabel("Hola DAM1",JLabel.CENTER);
		frame.add(jl);
		//frame.setSize(400,300);
		// dimensiona la ventana según el tamaño de sus componentes
		frame.pack();
		frame.setLocationRelativeTo(null);
	}
}
