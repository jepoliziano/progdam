import java.awt.*;
import javax.swing.*;

public class ejercicio extends JFrame
{
	public void paint(Graphics g)
	{
		g.setColor(new Color(0,255,0));
		g.drawRect(100,50,300,400);
		g.setColor(new Color(255,0,0));
		g.fillOval(150,150,100,100);
	}

	public static void main(String args[])
	{
		ejercicio ej=new ejercicio();
		ej.setSize(500,500);
		ej.setVisible(true);
		ej.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
}
