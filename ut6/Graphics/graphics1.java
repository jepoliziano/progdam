import java.awt.*;
import javax.swing.*;
 
public class graphics1 extends JFrame
{	 
	// constructor sets window's title bar string and dimensions
	public graphics1()
	{
		super( "Using colors" );
		setSize( 700, 350 );
		setVisible( true );
	 }

	 // draw rectangles and Strings in different colors
	 public void paint( Graphics g )
	 {
		 // call superclass's paint method
		 super.paint( g );
		 
		 // set new drawing color using integers
		 g.setColor( new Color( 255, 0, 0 ) );
		 g.fillRect( 25, 25, 100, 20 );
		 g.drawString( "Current RGB: " + g.getColor(), 130, 40 );
		// set new drawing color using floats
		 g.setColor( new Color( 0.0f, 1.0f, 0.0f ) );
		 g.fillRect( 25, 50, 100, 20 );
		 g.drawString( "Current RGB: " + g.getColor(), 130, 65 );
		 
		 // set new drawing color using static Color objects
		 g.setColor( Color.BLUE );
		 g.fillRect( 25, 75, 100, 20 );
		 g.drawString( "Current RGB: " + g.getColor(), 130, 90 );
		 
		 // display individual RGB values
		 Color color = Color.MAGENTA;
		 g.setColor( color );
		 g.fillRect( 25, 100, 100, 20 );
		 g.drawString( "RGB values: " + color.getRed() + ", " +
		 color.getGreen() + ", " + color.getBlue(), 130, 115 );
		int radio = 50;
		g.fillOval(100,200,radio,radio);
		g.drawOval(300,150,radio,2*radio);
		g.draw3DRect(25,200,50,100,false);
	 } // end method paint
	 
	 // execute application
	 public static void main( String args[] )
	 {
		 graphics1 application = new graphics1();
		 application.setDefaultCloseOperation( JFrame.EXIT_ON_CLOSE );
	 }
 
} // end class ShowColors
