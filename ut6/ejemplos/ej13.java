import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;

public class ej13 {
	static final int MIN = 0;
	static final int MAX = 1000;
	static final int INIT = 500;
	private static JLabel label = new JLabel("1 Euro son en dolares:");
	private static JLabel lbleuros = new JLabel("Euros:");
	private static JLabel lbldolares = new JLabel("Dolares:");
	private static JFrame frame = new JFrame("Conversor Euros - Dolares");
	private static JPanel panel1 = new JPanel();
	private static JPanel panel2 = new JPanel();
	private static JPanel panel3 = new JPanel();
	private static JTextField txteuro = new JTextField("0");
	private static JTextField txtdolar = new JTextField("0");
	private static JTextField txtcambio = new JTextField("1.36");
	private static JSlider sliderdolar = new JSlider(JSlider.HORIZONTAL,MIN,MAX,INIT);
	private static JSlider slidereuro = new JSlider(JSlider.HORIZONTAL,MIN,MAX,INIT);
	
	// responderá a eventos de cambio en los recuadros de texto
	public static void cambiotexto(ActionEvent e) {
		// Compruebo qué recuadro de texto ha cambiado
		if ( e.getSource() == txteuro ){
			// leo su nuevo valor
			float icambio=Float.parseFloat(txteuro.getText());
			// lo convierto según el cambio multiplicando por 100 para no perder precisión
			icambio=100*icambio*Float.parseFloat(txtcambio.getText());
			// lo redondeo
			icambio = Math.round(icambio);
			// y le divido 100 para dejarlo en su valor correcto
			icambio = icambio/100;
	// cambio el otro recuadro de texto para actualizarlo al valor obtenido en el anterior cálculo
            txtdolar.setText(String.valueOf(icambio));
            //cambiar los slider
            sliderdolar.setValue(Math.round(Float.parseFloat(txtdolar.getText())));
            slidereuro.setValue(Math.round(Float.parseFloat(txteuro.getText())));
		}
		if ( e.getSource() == txtdolar ){
			System.out.println("dentro");
			float icambio=Float.parseFloat(txtdolar.getText());
			icambio = 100*icambio/Float.parseFloat(txtcambio.getText());
			icambio = Math.round(icambio);
			icambio = icambio/100;
            txteuro.setText(String.valueOf(icambio));
		}
	}

	// responderá a los eventos de desplazamiento de los JSlider
	public static void mueveSlider(ChangeEvent e) {
		int valor;
		JSlider obj=(JSlider)e.getSource();
		System.out.println(obj.getValueIsAdjusting());
		System.out.println(obj.getValue());
		// cuando se acabe de desplazar el JSlider
		if (!obj.getValueIsAdjusting()) {
			System.out.println(obj.getValue());
			valor = (int)obj.getValue();
			if (obj == sliderdolar){
				txtdolar.setText(String.valueOf(valor));
				float icambio=100*valor/Float.parseFloat(txtcambio.getText());
				icambio=Math.round(icambio);
				icambio=icambio/100;
				//cambiar el txteuro
				txteuro.setText(String.valueOf(icambio));
				//cambiar el slidereuro
				int i = Math.round(icambio);
				slidereuro.setValue(i);
			}
			if (obj == slidereuro){
				txteuro.setText(String.valueOf(valor));
				float icambio=100*valor*Float.parseFloat(txtcambio.getText());
				icambio=Math.round(icambio);
				icambio=icambio/100;
				//cambiar el txtdolar
				txtdolar.setText(String.valueOf(icambio));
				//cambiar el sliderdolar
				int i = Math.round(icambio);
				slidereuro.setValue(i);
			}
		}
	}

	public static void colocaelementos()
	{
		frame.getContentPane().add(panel1);
		frame.getContentPane().add(panel2);
		frame.getContentPane().add(panel3);
		
		slidereuro.setBorder(BorderFactory.createTitledBorder("Euros"));
		slidereuro.setMajorTickSpacing(200);
		slidereuro.setMinorTickSpacing(100);
		// ponemos las marcas mayores y menores
		slidereuro.setPaintTicks(false);
		// mostramos los valores de esas marcas
		slidereuro.setPaintLabels(false);
		//slidereuro.disable();

		sliderdolar.setBorder(BorderFactory.createTitledBorder("Dolares"));
		sliderdolar.setMajorTickSpacing(200);
		sliderdolar.setMinorTickSpacing(100);
		sliderdolar.setPaintTicks(true);
		sliderdolar.setPaintLabels(true);
		sliderdolar.disable();

		// incorporo componentes al panel izquierdo
		panel1.add(lbleuros);
		panel1.add(txteuro);
		panel1.add(slidereuro);
		// incorporo componentes al panel central
		panel2.add(label);
		panel2.add(txtcambio);
		// incorporo componentes al panel derecho
		panel3.add(lbldolares);
		panel3.add(txtdolar);
		panel3.add(sliderdolar);
		frame.addWindowListener(new WindowAdapter() {
			    			public void windowClosing(WindowEvent e) {
						System.exit(0);
			    			}
					});
		txteuro.addActionListener(new ActionListener() {
			    public void actionPerformed(ActionEvent e) {
						cambiotexto(e);
			    }
		});
		txtdolar.addActionListener(new ActionListener() {
			    public void actionPerformed(ActionEvent e) {
						cambiotexto(e);
			    }
		});

		frame.setLayout(new FlowLayout());
		panel1.setLayout(new GridLayout(0,1));
		panel2.setLayout(new GridLayout(0,1));
		panel3.setLayout(new GridLayout(0,1));
		frame.pack();
		frame.setVisible(true);

    }
    public static void main(String[] args) {
       try {
            UIManager.setLookAndFeel(
                UIManager.getCrossPlatformLookAndFeelClassName());
        } catch (Exception e) { }
        colocaelementos();
    }
}

