import javax.swing.*;          
import java.awt.*;
import java.awt.event.*;

public class ej12
{
	private static JLabel label = new JLabel("--");
	private static JButton btnlimpia = new JButton("Limpia");
	private static JButton btnescribe = new JButton("Escribe");

	public static void acciones(ActionEvent e) {
		// Llamo al método getSource que me devolverá el objeto sobre el cual se ha generado el evento
		Object obj=e.getSource();
		if (obj == btnlimpia){
			label.setText("");
		}
		if (obj == btnescribe){
			label.setText("Hola Mundo");
		}
	}
    public static void main(String[] args) {
        try {
            UIManager.setLookAndFeel(
                UIManager.getCrossPlatformLookAndFeelClassName());
        } catch (Exception e) { }
        JFrame frame = new JFrame("Controlando eventos");
        btnlimpia.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
				acciones(e);
            }
        });
        btnescribe.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
				acciones(e);
            }
        });
        frame.getContentPane().add(label);
        frame.getContentPane().add(btnlimpia);
        frame.getContentPane().add(btnescribe);
        frame.addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                System.exit(0);
            }
        });
        frame.setLayout(new GridLayout(0,1));
        frame.pack();
	// CENTRAMOS LA VENTANA CON EL VALOR NULL
	frame.setLocationRelativeTo(null);
        frame.setVisible(true);
    }
}

