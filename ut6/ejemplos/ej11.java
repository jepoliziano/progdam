import javax.swing.*;

public class ej11
{
	public static void main(String[] args) 
	{
		JFrame frame = new JFrame("Ventana Hola Mundo");
		frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		// SE CREA LA ETIQUETA, INICIALMENTE INDEPENDIENTE DE LA VENTANA
		JLabel label= new JLabel("Hola Mundo");
		// SE AÑADE LA ETIQUETA A LA VENTANA
		frame.getContentPane().add(label);
		// ESTABLECE EL TAMAÑO DE LA VENTANA POR DEFECTO AL VALOR MÍNIMO DE SUS COMPONENTES
		frame.pack();
		// CENTRAMOS LA VENTANA CON EL VALOR NULL
		frame.setLocationRelativeTo(null);
		// ADEMÁS, HAY QUE HACERLO VISIBLE
		frame.setVisible(true);
	}
}
