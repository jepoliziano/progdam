/*2. Crea un programa de gestión de discos de música. Para ello, define una clase Disco (o reutiliza la creada en la unidad 2), sabiendo que para cada disco queremos registrar el título, grupo o artista, precio. Añade a la clase todos los constructores y métodos que consideres útiles o necesarios.

 El programa debe crear un array para 10 discos inicialmente vacío y mostrar el siguiente menú:

	1. Alta de disco
	2. Mostrar todos los discos
	3. Modifificar disco existente
	4. Ordenar por precio
	5. Ordenar por título
	0. Salir 
*/

import java.util.Scanner;

class disco
 {
 	private String titulo;
 	private String grupo;
 	private double precio=15;
 	// métodos: constructores, setters, getters y otros
 	public disco() { titulo = "Street Legal"; grupo = "Bob Dylan"; }
 	public disco(String t, String g) { titulo = t; grupo = g; }
 	public void setTitulo(String t) { titulo = t; }
 	public void setGrupo(String g) { grupo = g; }
	public void setPrecio(double p) { precio = p; }
 	public String getTitulo() { return titulo; }
 	public String getGrupo() { return grupo; }
	public double getPrecio() { return precio; }
 	public void muestraDisco()
 	{
 		System.out.println("Título: " + titulo + ", grupo: " + grupo + ", precio: " + precio);
 	}
 	public void oferta()
 	{
 		precio *= 0.8;		// precio = precio*0.8;
 	} 		
	public void modificaDisco() 
	{
		Scanner ent = new Scanner(System.in);
		System.out.println("Modifica el disco 2 (título, grupo, precio):");
		String t = ent.nextLine();
		titulo = t;
		String g = ent.nextLine();
		grupo = g;
		double p = ent.nextDouble();
		precio = p;
	}
 }
public class ex2
{
	private final static int TAM = 3;
	public static void main(String[] args)
	{
		Scanner ent = new Scanner(System.in);

		disco d[] = new disco[TAM];

		disco aux = new disco();

		for (int i=0; i < TAM; i++)
			d[i] = new disco();

		d[0].setTitulo("Runaway");
		d[0].setGrupo("Bon Jovi");
		d[0].setPrecio(10);
		d[1].setTitulo("Destroyer");
		d[1].setGrupo("Kiss");
		d[1].setPrecio(20);

		for (int i=0; i < TAM; i++)
			d[i].muestraDisco();

		d[1].modificaDisco();

		for (int i=0; i < TAM; i++)
			d[i].muestraDisco();

		//ordeno por precio

		double precio1 = d[0].getPrecio();
		double precio2 = d[1].getPrecio();
		double precio3 = d[2].getPrecio();	

		for (int tope = TAM-2; tope >= 0; tope--)
		{
			System.out.println("Empezando por el más barato...");			

			for (int i=0; i <= tope; i++)
			{	
				if (precio1 > precio2)
				{
					aux = d[0];
					d[0] = d[1];
					d[1] = aux;
				}
				if (precio2 > precio3)
				{
					aux = d[1];
					d[1] = d[2];
					d[2] = aux;
					if (precio1 > precio2)
					{
						aux = d[0];
						d[0] = d[1];
						d[1] = aux;
					}
				}
			}
		}	
		
			System.out.println("El contenido del array es:");
	
			for (int i=0; i < TAM; i++)
				d[i].muestraDisco();

		//ordeno por orden alfabético

		d[0].setTitulo("Runaway");
		d[0].setGrupo("Bon Jovi");
		d[0].setPrecio(10);
		d[1].setTitulo("Destroyer");
		d[1].setGrupo("Kiss");
		d[1].setPrecio(20);
		d[2].setTitulo("Street Legal");
		d[2].setGrupo("Bob Dylan");
		d[2].setPrecio(15);

		char inicial1 = d[0].getTitulo().charAt(0);
		char inicial2 = d[1].getTitulo().charAt(0);
		char inicial3 = d[2].getTitulo().charAt(0);

		for (int tope = TAM-2; tope >= 0; tope--)
		{
			System.out.println("En orden alfabético...");			

			for (int i=0; i <= tope; i++)
			{	
				if (inicial1 > inicial2)
				{
					aux = d[0];
					d[0] = d[1];
					d[1] = aux;
				}
				if (inicial2 > inicial3)
				{
					aux = d[1];
					d[1] = d[2];
					d[2] = aux;
					if (inicial1 > inicial2)
					{
						aux = d[0];
						d[0] = d[1];
						d[1] = aux;
					}
				}
			}	
		}	
			System.out.println("El contenido del array es:");
	
			for (int i=0; i < TAM; i++)
				d[i].muestraDisco();	
	}
}
