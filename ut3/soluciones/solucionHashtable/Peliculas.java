
import java.util.Hashtable;

/**
 * Clase que nos permite crear y editar colecciones de películas mediante el uso de Hashtable
 * @author Jose Rodriguez
 */

public class Peliculas{
	private String lis;
	private Hashtable<Integer,String> peliculas = new Hashtable<Integer,String>();
	
  //Métodos públicos
  
  /**
   *Metodo que nos permite insertar una película en la lista
   *@param pelicula nombre de la película a introducir
   */
	public void insertarPelicula(String pelicula){
		peliculas.put(peliculas.size()+1,pelicula);
	}

  /**
   *Metodo que nos permite eliminar una película de la lista usando su número
   *@param numPelicula número de la película a eliminar
   */
	public void eliminarPelicula(Integer numPelicula){
		//Necesitamos bajar la posición de todas las películas por encima de la posición de la que deseemos eliminar
   /* for(int i=numPelicula;i<peliculas.size();i++)
			peliculas.replace(i,peliculas.get(i+1));*/		
    	peliculas.remove(numPelicula); //Eliminamos la última posición
	}
	
  /**
   * Metodo toString, nos permite mostrar la lista completa usando el objeto de la clase como String. Devuelve una advertencia si la lista aun no tiene Películas
   * @return lista Lista completa y formateada de las películas  actuales
   */
/*	public String toString(){
		if(!vacia()){ 
			lis = new String();

            for(int clave=1;clave<=peliculas.size();clave++){
                lis += ""+ clave + ")" + peliculas.get(clave) +"\n";
            }

			//peliculas.forEach((clave,valor) -> {lis += ""+ clave + ")" + valor +"\n"; });
			return "Listado de películas:\n"+lis;
		}
		else 
			return "No hay películas disponibles!";
	}*/
	public String toString()
	{
		/* String s=null;
		Enumeration claves = peliculas.keys();
		Enumeration valores = peliculas.values();
		while(claves.hasMoreElements())*/
		return peliculas.toString();
	}
	
  //Metodos privados
  
  /**
   *Metodo que nos permite comprobar si la lista está vacía
   *@return estado devuelve true si la lista está vacía
   */
/*	private boolean vacia(){
		return peliculas.size()==0;
	}*/
}
