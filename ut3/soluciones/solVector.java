/* 1. Realiza un ejercicio (preferentemente modular ) que permita almacenar nombres (personas o ciudades, ..., lo que prefieras) en un objeto Vector, con métodos que permitan cargar sus valores de teclado, mostrar todos los nombres por pantalla, etc. El objeto Vector será creado con una capacidad inicial de 10 e incrementos de 5 en 5.
 El programa guardará todos los nombres que quiera introducir el usuario hasta acabar con una línea vacía, mostrará todos los nombres almacenados, así como el tamaño y capacidad del vector, para acabar pidiendo un nombre que habrá de eliminar del Vector. */
 
import java.util.*;

public class solVector
{
	private static Vector<String> v = new Vector<String>(10,5);

	public static void main(String[] args)
	{
		System.out.println("Introduce una ciudad:");
		String s = System.console().readLine();
		//v.add(s);
		int opcion = 0;

		while (!s.isEmpty()) {
			v.add(s);
			System.out.println("\nMENU\n" + "\n1.Añadir nueva ciudad\n" + "\n2.Mostrar vector\n" + "\n3.Eliminar ciudad\n" + "\n4.Consultar ciudad\n" + "\n5.Tamaño y capacidad actuales\n");
			opcion = Integer.parseInt(System.console().readLine());
			switch(opcion) {
				case 1: break;
				case 2: muestraVector();break;
				case 3: System.out.println("Indica ciudad a eliminar:");
						s = System.console().readLine();
						eliminaCiudad(s);break;
				case 4: consultaCiudad();break;
				case 5: sizeCapacity();break;
			}
			System.out.println("Introduce una ciudad:");
			s = System.console().readLine();
			//v.add(s);
		}
	}
	public static void muestraVector() {
		Enumeration vEnum = v.elements();

		while (vEnum.hasMoreElements())
			System.out.println("Los valores del vector son: " + vEnum.nextElement());
	}
/*	public static void eliminaCiudad() {
		System.out.println("Introduce la posición de la ciudad a eliminar:");
		int n = Integer.parseInt(System.console().readLine());

		v.removeElementAt(n);
	} */
	public static void eliminaCiudad(String s) {
		v.remove(s);
	}
	public static void consultaCiudad() {
		System.out.println("Introduce la posición de la ciudad de consulta:");
		int n = Integer.parseInt(System.console().readLine());

		System.out.println("El elemento de la posición " + n + " es " + v.elementAt(n));
	}
	public static void sizeCapacity() {
		System.out.println("El tamaño del vector es: " + v.size());
		System.out.println("La capacidad del vector es: " + v.capacity());
	}
}
