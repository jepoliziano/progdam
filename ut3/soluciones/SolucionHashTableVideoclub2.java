/*Realiza un programa que, mediante la clase Hashtable, permita gestionar 
películas de una videoteca o videoclub. Para ello, se utilizarán entradas 
de tipo <Integer,String> tal que cada película tenga un número de película 
y su título.

	El programa permitirá:
	- introducir películas desde teclado
	- listar todas las películas
	- eliminar una película a partir de su número
	- consultar el título de una película a partir de su número	
	VERSIÓN CON MENÚ
//import java.util.Hashtable; */
import java.util.*;


public class SolucionHashTableVideoclub2{

	public static void main(String args[]){
		int lista=1, opcion; String pelicula="i"; 
		Hashtable<Integer,String> videoclub = new Hashtable<Integer,String>(); //creo el hashTable
//		videoclub.put(1,"Matrix");
//		videoclub.put(8,"Pulp Fiction");
//		videoclub.put(4,"Trainspotting");
		do
		{
			System.out.println("GESTIÓN VIDEOCLUB");
			System.out.println("1. Introducir una película desde teclado");
			System.out.println("2. Listar todas las películas");
			System.out.println("3. Eliminar una película a partir de su número");
			System.out.println("4. Consultar el título de una película a partir de su número");
			System.out.println("5. Salir del programa");
			System.out.println("Introduce la opción");
			opcion = Integer.parseInt(System.console().readLine());
			switch (opcion)
			{
				case 1: System.out.println("Introduce Nº de película: ");
						lista = Integer.parseInt(System.console().readLine());
						System.out.println("Introduce el título de la película: ");
						pelicula = System.console().readLine();
						anyadePeli(lista,pelicula,videoclub);
						break;
				case 2: recorreHash(videoclub); break;
				case 3: System.out.println("¿Qué número de película quieres eliminar?");
						lista = Integer.parseInt(System.console().readLine());
						deleteHash(lista,videoclub); break;
				case 4: System.out.println("Introduce el número de la pelicula a consultar");
						lista = Integer.parseInt(System.console().readLine());
						System.out.println("El título de esa película es " + consulta(lista,videoclub)); break;
				case 5: System.exit(0);
			}
		} while (true);

/*		listaHash(videoclub);

		recorreHash(videoclub);*/

	}

	public static void anyadePeli(int n, String t, Hashtable v)
	{
		v.put(n,t);
	}
	
/*	public static void listaHash(Hashtable tabla){
		System.out.println("El tamaño actual de la lista es: " + tabla.size() + " y contiene: " + tabla); //.size dirá el número de elementos (valor con su película)que hay en la lista.
		
	}*/
	
	public static void deleteHash(int num,Hashtable tabla){
		tabla.remove(num);
	}
	
	public static String consulta(int con,Hashtable tabla){
		
		return (String)(tabla.get(con));
	}

	public static void recorreHash(Hashtable tabla){

		Enumeration claves = tabla.keys(); //tenemos que crear un Enumaration del hashTable de keys y elements para poder recorrer la tabla, necesita importar otra clase
		Enumeration valores = tabla.elements();
		while(claves.hasMoreElements()){
			System.out.println("Número:" + claves.nextElement() + " película: " + valores.nextElement());

		}
	}
}
