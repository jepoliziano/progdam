public class ej10
{
	private static int[]	lista;
	final static int		POS		= 100;	// número de posiciones del array
	final static int		LIMITE	= 100;	// Números entre 1..Límite

	public static int getaleatorio()
	{

		return (int) (Math.random() * LIMITE + 1);
	}

	public static void ordena(int array[])
	{

		int aux;

		for (int i = array.length; i > 0; i--)
		{

			for (int j = 0; j < i - 1; j++)
			{

				if (array[j] > array[j + 1])
				{

					aux = array[j + 1];

					array[j + 1] = array[j];

					array[j] = aux;

				}

			}

		}
	}

	public static void muestra()
	{

		for (int i = 0; i < POS; i++)
		{

			System.out.print(lista[i] + " ");

		}
	}

	public static boolean encuentra(int num)
	{

		for (int i = 0; i < POS; i++)
		{

			if (lista[i] == num)
				return true;

			if (lista[i] > num)
				return false;

		}

		return false;
	}

	public static void main(String[] args)
	{

		lista = new int[POS];

		for (int i = 0; i < POS; i++)
		{

			lista[i] = getaleatorio();

		}

		muestra();// se muestra el vector desordenado

		System.out.println("");

		ordena(lista); // ordenación por burbuja

		muestra();// se muestra el vector ordenado
		System.out.println("");

		for (int i = 1; i <= POS; i++)
		{

			if (!encuentra(i))
			{

				System.out.println("El número " + i + " no está en el array");

			}

		}

		System.out.println("");
	}
}
