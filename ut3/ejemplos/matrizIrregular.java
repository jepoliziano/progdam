// Ejemplo de MATRIZ IRREGULAR (double)

import java.util.Scanner;

public class matrizIrregular
{
	private static final int F = 3;	// número de filas
//	private static final int C = 4;	// número de columnas
	public static void main(String[] args) {
		Scanner ent = new Scanner(System.in);
		double matriz[][] = new double[F][];	// matriz irregular
		for (int i=0 ; i < F ; i++)
			matriz[i] = new double[i+1];	// creo el vector para CADA FILA
		// cargar valores desde teclado
		for (int f = 0 ; f < F ; f++)
			for (int c = 0 ; c < matriz[f].length ; c++)
				matriz[f][c] = ent.nextDouble();
		for (int f = 0 ; f < F ; f++)
		{
			for (int c = 0 ; c < matriz[f].length ; c++)
				System.out.print(matriz[f][c] + "\t");
			System.out.println("");
		}
		//matriz[0][1]=6;	daría excepción ArrayIndexOutOfBoundsException
		System.out.println(matriz.length);
		
		
		System.exit(0);
	}
	

}
