// Ejemplo de búsqueda lineal booleana

import java.util.Scanner;

public class busqLinealBool
{
	public static void main(String args[])
	{
		final int TAM = 5;
		Scanner ent = new Scanner(System.in);
		// declaro el array de 5 valores numéricos double
		double nums[] = new double[TAM];
		// leo valores desde teclado
		for (int i = 0 ; i < TAM ; i++)
			nums[i] = ent.nextDouble();
		// muestro el contenido del array en pantalla
		for (int i = 0 ; i < TAM ; i++)
			System.out.print(nums[i] + "\t");
		System.out.println("¿Qué valor quieres buscar en el array?");
		double n = ent.nextDouble();
		if (busquedaLinealBooleana(n,nums))
			System.out.println("SI se encuentra en el array");
		else
			System.out.println("NO se encuentra en el array");
	}
	
	public static boolean busquedaLinealBooleana(double vb, double array[])
	{
		int i;
		for(i = 0 ; (array[i]!= vb) && (i < array.length) ; i++);
		/* o también:
			for (int i = 0 ; i < array.length ; i++)
				if (array[i] == vb) return true; */
		if (array[i] == vb)	// he salido del bucle porque lo he encontrado
			return true;
		else	// he salido del bucle porque he llegado al final
			return false;
	}
}
