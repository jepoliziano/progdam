// Ejemplo de array creado a partir de una lista de valores

public class arrayBasico2
{
	private final static int TAM = 5;
	public static void main(String args[])
	{
		// declaro el array de 5 valores numéricos double
		double nums[] = {6,3,9,11,4};
		// muestro el contenido del array en pantalla
		for (int i = 0 ; i < TAM ; i++)
			System.out.print(nums[i] + "\t");
			
	}
}
