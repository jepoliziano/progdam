/* 12. Los números de Fibonacci son los miembros de una secuencia en la que cada numero es igual a la suma de los dos anteriores. En otras palabras, Fi = Fi-1 + Fi-2, donde Fi es el i-ésimo nº de Fibonacci, y siendo F1=F2=1. Por tanto:
	F5 = F4 + F3 = 3 + 2 = 5, y así sucesivamente.
	Escribir un programa que determine los n primeros números de Fibonacci, siendo n un valor introducido por teclado.
	El programa deberá realizarse con cálculo recursivo.	 */

import java.util.Scanner;

public class ej12
{
	public static void main(String[] args)
	{
		int num;
		
		Scanner ent = new Scanner(System.in);
		System.out.println("Introduce un entero positivo:");
		num = ent.nextInt();
		System.out.println("Los primeros " + num + " valores en la serie de Fibonacci son:");
		for (int i=1 ; i <= num ; i++)
			System.out.println(fibonacci(i));
		System.exit(0);
	}
	// el método retornará el valor, en la posición indicada por el parámetro, para la serie de Fibonacci
	public static int fibonacci(int n)
	{
		if (n > 2)
			return fibonacci(n-1) + fibonacci(n-2);
		else
			return 1;
			
	}
}
	
	
	
