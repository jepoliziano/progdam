//Programa que determina si el año introducido es bisiesto o no.
public class ej29 
{
    public static void main (String[] args)
    {
        int year = leeNumero("Introduce un año para saber si es bisiesto");
        System.out.println(esBisiesto(year));
    }
    private static String esBisiesto (int year)
    {
        if (year % 4 == 0 && year % 100 != 0 || year % 400 == 0)
        {
            return "el año " + year + " es bisiesto";
        }
            return "el año " + year + " no es bisiesto";
    }
    private static int leeNumero(String mensaje)
    {
        System.out.println(mensaje);
        return stringToNumber(System.console().readLine());
    }
    private static int stringToNumber(String cadena)
    {
        try
        {
            return Integer.parseInt(cadena);
        }
        catch(NumberFormatException nfe)
        {
            return leeNumero("¡¡ERROR!! Vuelve a introducir un nuevo año:");
        }
    }
}
