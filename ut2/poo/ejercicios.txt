1. Crear una clase Reloj (hora,minutos,segundos) que permita al menos:

 - crear relojes con hora inicial las 12 del mediodía
 - crear relojes con hora inicial a elegir
 - cambiar sólo la hora, sólo los minutos o sólo los segundos (setters)
 - obtener sólo el valor de hora, o de minutos o de segundos (getters)
 - obtener los segundos transcurridos desde las 12 de la medianoche()
 - añadir una cantidad de tiempo expresada en segundos (vigila que los minutos o segundos no excedan de 59, ni las horas de 23).
 	Ejemplo:
 	si el reloj marca las 10:35:24 y se le añaden 1810 segundos (media hora y 10 segundos) quedará en 11:05:34
 - ... (otras operaciones que se te ocurran útiles)
 
 Crea un programa que instancie relojes poniendo a prueba toda la clase anterior.
 
2. Crea una clase Musico que incluya 2 atributos (nombre del músico e instrumento, ambos de tipo String). La clase incluirá también, al menos:

 - un constructor que inicialice el músico con un nombre y un instrumento inicial.
 - constructor de copia.
 - un método muestraMusico (o toString()) que muestre los datos del músico por pantalla.
 - los correspondientes métodos "setters" y "getters" (setNombre,setInstr,getNombre,getInstr) que cambien el instrumento o el nombre (métodos setXxx), o devuelvan esos mismos valores (métodos getXxx).

	Realiza un programa que, utilizando la clase Musico(*), defina al menos dos objetos de este tipo, muestre sus datos, modifique algún dato de cada uno de ellos y acabe mostrando sus nombres, instrumentos y el total de músicos.

(*) utiliza la clase Musico del ejercicio anterior, añadiéndole un tercer atributo: número de músicos (este atributo se incrementará en una unidad para cada músico nuevo que se cree).

3. Crea un programa de gestión de discos de música. Para ello, define una clase Disco, sabiendo que para cada disco queremos registrar el título (String), grupo o artista (String), precio (por defecto, 15 euros pero algún disco puede estar en oferta, rebajándolo un 20%). Añade a la clase los constructores y métodos que consideres útiles o necesarios.

 El programa debe crear 3 discos, con valores de títulos y grupos introducidos desde teclado por el usuario. Además el programa pondrá en oferta un único disco, elegido al azar con Math.random(). Finalmente, mostrará todos los discos con sus precios.
 
4. Modifica la clase Disco para cambiar el atributo nombre o artista (previamente de tipo String) por un atributo de tipo Musico (clase definida para el ejercicio anterior). Al mostrar los discos en el programa, has de utilizar el método muestraMusico().

5. Define la clase Dado con:

	- un atributo "valor" (que variará entre 1 y 6)
	- constructor general con valor indicado como parámetro
	- constructor general con valor al azar
	- setter y getter (setValor y getValor)
	- un método "tirada" que cambia el valor del dado al azar
	- otro método "muestra" que dibuje el dado en pantalla con asteriscos
		
Realiza a continuación un programa que ponga a prueba todo lo anterior. Habrás de crear al menos 2 objetos Dado, cada uno con un constructor diferente.
