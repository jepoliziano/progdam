// Programa que instancia rectángulos (crea objetos de esa clase)

public class usaFiguras
{
	public static void main(String[] args)
	{
		// creamos dos objetos rectangulo
		rectangulo r = new rectangulo();	// utilizo el primer constructor
		System.out.println(r);	// llamada a r.toString()
		rectangulo r2 = new rectangulo();
		System.out.println(r2);
		rectangulo r3 = r;	// copia superficial
		System.out.println(r3);	// llama al toString() sobreescrito
		//System.out.println(r3.toString());	
		
		System.exit(0);
	}
}
