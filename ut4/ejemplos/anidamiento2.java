//  ejemplo de clase anidada estática
class A
{
	private B b;
	public A() { b= new B();b.num=5;}
	public void setB(B b) { this.b=b;}
	// ahora esta clase es estática
	static class B
	{
		private int num;
	}
}

class anidamiento2
{
	public static void main(String args [])
	{
		A a=new A();
		//B b = new B();
		A.B ab = new A.B();
		//a.setB(new a.B());
		//A.B ab2 = a.new B();
	}
}
