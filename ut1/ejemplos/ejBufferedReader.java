// ejemplo de lectura desde teclado con la clase BufferedReader

import java.io.InputStreamReader;
import java.io.BufferedReader;
import java.io.IOException;

public class ejBufferedReader
{
	public static void main(String[] args) throws IOException
	{
		// creo el objeto BufferedReader que requiere previamente de un objeto InputStreamReader
		InputStreamReader isr = new InputStreamReader(System.in);
		BufferedReader ent = new BufferedReader(isr);
		// ejemplo de lectura de texto con BufferedReader
		System.out.println("Introduce un texto:");
		String s = ent.readLine();
		System.out.println("El texto introducido es " + s);
		// ejemplo de lectura numérica con BufferedReader
		System.out.println("Introduce un número:");
		double n = Double.parseDouble(ent.readLine());
		System.out.println("El número introducido es " + n);
		
		System.exit(0);
	}
}
