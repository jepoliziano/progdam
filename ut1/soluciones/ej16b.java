/* 16. Programa que indique el máximo y el mínimo de un conjunto de notas introducidas desde teclado.
 La serie de notas acabará con un negativo */
 
 // ¡OJO!: Esta solución no acaba al introducir un negativo después de un valor superior a 10
 
import java.util.*;

public class ej16b
{
	public static void main(String[] args)
	{
		double nota = 0, max = -1, min = 11;
		boolean continua = true, algunaNota = true;
		Scanner entrada = new Scanner(System.in);

		System.out.println("Introduce una nota, para acabar introduce un negativo: ");
		nota = entrada.nextDouble();
        // Primer filtro: comprobar que el usuario no introduzca un negativo de inicio, con los booleanos no entrará al bucle principal y no imprimará por pantalla el mensaje del final.
		if (nota < 0)
		{
			continua = false;
			algunaNota = false;
			System.out.println("Has elegido salir del programa.");
		}
        // Se repetirá hasta que se introduzca un -1
		while (continua)
		{
                    // Con el if y el do-while se consigue que el usuario introduzca un valor válido, y hasta que no lo introduzca se le volverá a preguntar.
					if ((nota < 0) || (nota > 10))	// NO válida
					{
						do
						{
							System.out.println("Debes introducir una nota entre 0 y 10, para acabar introduce un negativo:");
							nota = entrada.nextDouble();
						} while ((nota < 0) || (nota > 10));
					}
                    // Ahora seguro que la nota es válida, así que se inicia el algoritmo para guardar los valores min y max.
					if (max < nota)
						max = nota;
					if (nota < min)
						min = nota;
                    // Se termina el bucle preguntando la nota otra vez, si se introduce un negativo saldrá del bucle, y si no es un valor válido, volverá a preguntar.
					System.out.println("Introduce otra nota entre 0 y 10:");
					nota = entrada.nextDouble();
					if (nota < 0)
						continua = false;
			
		}
        // Se comprueba que el usuario no ha introducido un -1 al principio, y de ser así, no se muestra el mensaje final.
		if (algunaNota)
			System.out.println("La nota máxima es " + max + ", y la mínima es: " + min);			
	}
}
