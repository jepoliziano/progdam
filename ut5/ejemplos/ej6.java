import java.io.*;
public class ej6
{
	public static void main(String[] args)
	{
		FileInputStream fis=null;
		String s="";
		char c;
		try{
			fis=new FileInputStream("datos.txt");
			int size = fis.available();
			for (int i=0;i<size;i++){
				c=(char)fis.read();
				s=s+c;
			}
		}catch(IOException e){
			e.printStackTrace();
		}finally{
			System.out.println(s);
			try{
				fis.close();
			}catch(IOException e){
				e.printStackTrace();
			}
		}
	}
}

