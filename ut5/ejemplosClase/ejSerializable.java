/* Ejemplo de serialización de libros */

import java.io.*;

class libro implements Serializable
{
	private int cod;
	private String tit;
	private String aut;

	public libro(int c, String t, String a)
	{
		cod=c; tit=t; aut=a;
	} 
	public void setTitulo(String nt) { tit = nt;}
	public String toString() { return "Libro: " + cod + ", " + tit + ", " + aut;}
}

public class ejSerializable
{
	public static void main(String[] args) {
		libro l1,l2,l3=null;
		libro l=null;
		// SERIALIZACION
		try (
		FileOutputStream fos = new FileOutputStream("libros.objc");
		ObjectOutputStream oos = new ObjectOutputStream(fos);
		)
		{
			l = new libro(1,"Don Quijote","Cervantes");
			oos.writeObject(l);
			//System.out.println(l);
			l = new libro(2,"Tirant lo Blanc","Martorell");
			oos.writeObject(l);
			//System.out.println(l);
			l = new libro(3,"c","c");
			oos.writeObject(l);
			//System.out.println(l);
			// INCORRECTO!!
			/* l.setTitulo("Cien años de soledad");
			oos.writeObject(l); */
		}
		catch(IOException e)
		{
			System.err.println(e.getMessage());
		}
		//l1=l2=l3=null;
		// DES-SERIALIZACION
		try (
			FileInputStream fis = new FileInputStream("libros.objc");
			ObjectInputStream ois = new ObjectInputStream(fis);
		)
		{
			/* l1 = (libro)ois.readObject();
			System.out.println(l1);
			l2 = (libro)ois.readObject();
			System.out.println(l2);
			l3 = (libro)ois.readObject(); 
			System.out.println(l3); */
			while((l = (libro)ois.readObject())!= null)
			//{
				//l = null;
				
				System.out.println(l);
			//}
		}
		catch(IOException e)
		{
			if(e.getMessage()==null)
                System.out.println("Fin");
            else
                System.err.println(e.getMessage());
		}
		catch(ClassNotFoundException e2)
		{
			System.err.println(e2.getMessage());
		}
	}
}
