/* Ejemplo de PushBackReader, sustiturá 
	a=a+1;c++;b+=5;c=a+b;b++;
por
	a=a+1;c=c+1;b+=5;c=a+b;b=b+1;
*/

import java.io.*;

public class ejPushBackReader
{
	public static void main(String[] args) {
		String s = "a=a+1;c++;b+=5;c=a+b;b++;";
		StringReader sr = new StringReader(s);
		PushbackReader pbr = new PushbackReader(sr);
		try
		{
			int ultimo=pbr.read(),penultimo=0;
			while (ultimo != -1)
			{
				if (ultimo == '+')
					// lee el siguiente y comprueba si es otro '+'
					if ((ultimo = pbr.read()) == '+')
						System.out.print("="+(char) penultimo+"+1");
					else
					{
						// si no era otro '+', lo devuelvo
						pbr.unread(ultimo);
						System.out.print('+');
					}
				else
					System.out.print((char)ultimo);
				penultimo=ultimo;
				ultimo = pbr.read();
			}
			System.out.println();
		}
		catch(IOException e)
		{
			System.err.println(e.getMessage());
		}
	}
}
