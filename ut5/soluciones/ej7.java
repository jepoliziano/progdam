/* 7. Realiza un programa que elimine todos los espacios en blanco repetidos en un fichero de texto. */

import java.io.*;

public class ej7{
	public static void main(String[] args){
		if (args.length > 0){
			File f = new File(args[0]), f2 = new File("nuevo"); 
			String s;
			try(
				BufferedReader br = new BufferedReader(new FileReader(f));
				FileWriter fw = new FileWriter(f2);
				)
			{
				s = br.readLine();
				while(s != null){
					s = eliminaEspacios(s);
					fw.write(s + "\n");
					s = br.readLine();
				}
				f2.renameTo(f);
			}
			catch(IOException e){
				System.err.println(e.getMessage());
			}
		} else
			System.out.println("Debes introducir el fichero como parámetro.\nForma de uso: java Ej7 fichero");
	}
	
	public static String eliminaEspacios(String s){
		
		String s2 = ""; boolean espacio = false;

		for(int i = 0; i < s.length(); i++){
			if(s.charAt(i) == ' '){
				if(!espacio)
					s2 += s.charAt(i);
				espacio = true;
			}
			else {
				s2 += s.charAt(i);
				espacio = false;
			}	
		}
//		s2=s.replace(" +"," ");
		return s2;
	}
}
