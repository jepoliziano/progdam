/*5. Se necesita crear una clase censura con un método
 aplicaCensura, a la que se le pase como parámetro el nombre
  de un fichero, y sustituya cada aparición de "Linux" por "Unix"
  en dicho fichero.
   No te preocupes por los cambios de línea si se pierden.
    Recomendaciones: 

 - utiliza la clase Scanner, con sus métodos next y hasNext.
 - crea un segundo fichero en el que vayas escribiendo cada palabra,
  leída del fichero original, distinta de "Linux",
   y escribiendo "Unix" cuando la palabra leída sea aquélla. 
 Al final, puedes eliminar el fichero original y renombrar el nuevo con el del original. */

 import java.io.*;
 import java.util.Scanner;
// import java.nio.files.Files;

class censura
{
	private String pBusq;
	private String pReemp;
	
	public censura(String b,String r)
	{
		pBusq = b;
		pReemp = r;
	}
	
	public void aplicaCensura(String sf)
	{
		String linea;
		File flect = new File(sf);
		File fesc = new File(sf + ".tmp");

		try (Scanner fent = new Scanner(flect);FileWriter fw = new FileWriter(fesc);)
		{
		 	while (fent.hasNext())
		 	{
		 		// leo una línea del fichero de partida
		 		linea = fent.nextLine();
		 		linea = linea.replace(pBusq,pReemp);
		 		// escribo esa línea al nuevo fichero
		 		fw.write(linea + "\n");
		 	}
		 }
		 catch (IOException e)
		 {
		 	System.err.println(e.getMessage());
		 }
		 //flect.delete();
		 /* 
		 Path tempFilePath = Paths.get(tempFile);
		 Files.move(tempFilePath, tempFilePath.resolveSibling(args[0]), REPLACE_EXISTING);
String tempFile = args[0] + "~";
			 */
		 fesc.renameTo(flect);
	}
}

 public class ej5
 {
 	public static void main(String[] args) {
 		if (0 == args.length)
 		{
 			System.out.println("Uso: java ej5 /ruta/al/fichero");
 			System.exit(0);
 		}
 		censura c = new censura("Linux","Unix");
 		c.aplicaCensura(args[0]);
	 	
	 		
 	}
 }
