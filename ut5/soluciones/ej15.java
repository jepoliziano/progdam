/* 15. Programa que admita las siguientes formas de uso:

	java ej15

Sin parámetros, mostrará los permisos de lectura, escritura y ejecución del directorio actual para el usuario que ejecute el programa.

	java ej15 /ruta/al/fichero/o/directorio

Con un parámetro, mostrará los permisos de lectura, escritura y ejecución del directorio indicado para el usuario que ejecute el programa (si se indica un fichero, se hará para el directorio que lo contiene)

	java ej15 /ruta/al/fichero/o/directorio [c|d|l]

Cuando se añada como segundo parámetro una c,d o l, éstos tendrán el significado de [c]reate (crear el directorio si no existe) o [d]elete (borrar el directorio, sólo si éste está vacío) o [l]ist  (listar el contenido del directorio)..
*/

import java.io.*;

public class ej15
{
	public static void main(String[] args) {
		String s=".";

		if (args.length > 1)	// al menos 2 parámetros
		{
			if ('c' == args[1].charAt(0))
			{	// crear
				File f = new File(args[0]);
				if (f.mkdir())
					System.out.println("Directorio creado");
				else
					System.out.println("No se ha conseguido crear el directorio");
			}
			if ('d' == args[1].charAt(0))
			{	// crear
				File f = new File(args[0]);
				if (f.delete())
					System.out.println("Fichero o directorio borrado");
				else
					System.out.println("No se ha conseguido borrar");
			}
			if ('l' == args[1].charAt(0))
			{	// crear
				/*File d = new File(args[0]);
				String noms[] = d.list();
					for(String entrada: noms)
						System.out.println(entrada);*/
				File d = new File(args[0]);
				File fs[] = d.listFiles();
					for(File entrada: fs)
						if (entrada.isDirectory())
							System.out.println("Subdirectorio: " + entrada.getName());
						else
							System.out.println("Fichero: " + entrada.getName());
			}
			System.exit(0);
		}
		if (args.length > 0)
		{
			File f = new File(args[0]);
			// si es un directorio
			if (f.isDirectory())
				s = args[0];
			else// si es fichero
				s = f.getParent();
		}
		File d = new File(s);

		System.out.println("Permiso de lectura = " + d.canRead());
		System.out.println("Permiso de escritura = " + d.canWrite());
		System.out.println("Permiso de ejecución = " + d.canExecute());
	}
}

